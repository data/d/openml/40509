# OpenML dataset: Australian

https://www.openml.org/d/40509

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Confidential. Donated by Ross Quinlan  
**Source**: [LibSVM] (https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html), [UCI](https://archive.ics.uci.edu/ml/datasets/Statlog+(Australian+Credit+Approval)) - 1987    
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html) 

**Australian Credit Approval**. This is the famous Australian Credit Approval dataset, originating from the StatLog project. It concerns credit card applications. All attribute names and values have been changed to meaningless symbols to protect the confidentiality of the data. 

This dataset was retrieved 2014-11-14 from the libSVM site. It was normalized to [-1,1] and converted to the ARFF format.

### Feature information

There are 6 numerical and 8 categorical attributes, all normalized to [-1,1]. The original formatting was as follows:  

A1: 0,1 CATEGORICAL (formerly: a,b)  
A2: continuous.  
A3: continuous.  
A4: 1,2,3 CATEGORICAL (formerly: p,g,gg)  
A5: 1, 2,3,4,5, 6,7,8,9,10,11,12,13,14 CATEGORICAL (formerly: ff,d,i,k,j,aa,m,c,w, e, q, r,cc, x)  
A6: 1, 2,3, 4,5,6,7,8,9 CATEGORICAL (formerly: ff,dd,j,bb,v,n,o,h,z)  
A7: continuous.  
A8: 1, 0 CATEGORICAL (formerly: t, f)  
A9: 1, 0 CATEGORICAL (formerly: t, f)  
A10: continuous.  
A11: 1, 0 CATEGORICAL (formerly t, f)  
A12: 1, 2, 3 CATEGORICAL (formerly: s, g, p)  
A13: continuous.  
A14: continuous.  
A15: 1,2 class attribute (formerly: +,-)  

### Relevant Papers

Ross Quinlan. "Simplifying decision trees", Int J Man-Machine Studies 27, Dec 1987, pp. 221-234. 

Ross Quinlan. "C4.5: Programs for Machine Learning", Morgan Kaufmann, Oct 1992

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40509) of an [OpenML dataset](https://www.openml.org/d/40509). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40509/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40509/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40509/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

